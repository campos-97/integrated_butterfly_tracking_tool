# -*- coding: utf-8 -*-
"""
Butterflies application - SIP-Lab.

Settings.py
Created: 2018/08/10
Author: Abraham Arias Chinchilla

This module keeps the settings from the configuration files.

Todo:
    * Check the return of enoguhImagesToCalibrate.
"""
import traceback

import numpy as np
import cv2


class Settings():
    """This .

    The __init__ method may be documented in either the class level
    docstring, or as a docstring on the __init__ method itself.

    Either form is acceptable, but the two should not be mixed. Choose one
    convention to document the __init__ method and be consistent with it.

    Note:
        Do not include the `self` parameter in the ``Args`` section.

    Args:
        msg (str): Human readable string describing the exception.
        code (:obj:`int`, optional): Error code.

    Attributes:
        msg (str): Human readable string describing the exception.
        code (int): Exception error code.

    """

    def __init__(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.settingsFileName = "./CalibrationMain_Module/data/configuration.xml"
        self.m = [0, 0, 0]
        self.d = [0, 0, 0]
        self.extrinsic_Translation = np.zeros((3,3))
        self.extrinsic_Rotation = np.zeros((3,3))
        self.cam_vec_type = [0, 0, 0]

        #This values are not used. Intead are read from settings file
        self.font = cv2.FONT_HERSHEY_SIMPLEX # font for displaying text (below)
        self.nIntrinsicImages = [0, 0, 0]
        self.nExtrinsicImages = [0, 0, 0]
        self.patternHeightSize = 1.7742
        self.patternWithSize = 1.7150
        #lineEdit_squareSizeHeight
        self.NImagesRequieredForCamera = 10
        self.ImagesCounter = [0,0,0]
        self.patternSize = (7, 6)
        self.tolerance = 5

        self.arUcoApos = [[150, 150], [230, 150], [190, 110], [190, 190]]

        self.readSettingsFile()

    def getCameraMatrix(self, id):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        return self.m[id]

    def getCameraDistorsion(self, id):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        return self.d[id]

    def saveCalibrationImages(self, cameraID, img, calibrationType):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        if(calibrationType == 1):
            path = './CalibrationMain_Module/data/ext_img/calib_cam' + \
                        str(cameraID)+'_img' + \
                        str(self.nExtrinsicImages[cameraID]) + '.png'
        else:
            #print(cameraID)
            #print(self.nIntrinsicImages)
            path = './CalibrationMain_Module/data/int_img/calib_cam' + \
                        str(cameraID)+'_img' + \
                        str(self.ImagesCounter[cameraID]) + '.png'

        imgArgb = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        cv2.imwrite(path, imgArgb)

        if(calibrationType == 1):
            self.nExtrinsicImages[cameraID] += 1
        else:
            #self.nIntrinsicImages[cameraID] += 1
            self.ImagesCounter[cameraID] += 1

        return 1

    def saveCamerasIntrinsicParams(self, cameraID, cam_matrix, cam_dist):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        #TODO: Change this path - DONE
        print("Whaat is going on? 8")
        fs = cv2.FileStorage("./CalibrationMain_Module/data/calib/camera" +
                             str(cameraID) + "CalibrationParams.xml",
                             cv2.FILE_STORAGE_WRITE)
        print("Whaat is going on? 9")
        fs.write("cam" + str(cameraID) + "_matrix", cam_matrix)
        fs.write("cam" + str(cameraID) + "_dist", cam_dist)
        print("Whaat is going on? 10")
        fs.release()
        print("Whaat is going on? 11")

    def saveCamerasExtrinsicParams(self, matrix_Translation, matrix_Rotation):
        """To comment.
        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.extrinsic_Translation = matrix_Translation
        self.extrinsic_Rotation = matrix_Rotation
        # TODO: Change this path - DONE
        fs = cv2.FileStorage("./CalibrationMain_Module/data/calib/cameraCalibrationParams.xml",
                             cv2.FILE_STORAGE_WRITE)
        fs.write("cams_A_B_translation", matrix_Translation[0])
        fs.write("cams_A_B_rotation", matrix_Rotation[0])
        fs.write("cams_B_C_translation", matrix_Translation[1])
        fs.write("cams_B_C_rotation", matrix_Rotation[1])
        fs.write("cams_A_C_translation", matrix_Translation[2])
        fs.write("cams_A_C_rotation", matrix_Rotation[2])

        fs.release()

    def setCameraProtocalType(self, cam_vec_type):
        self.cam_vec_type = cam_vec_type
        return 1

    def getPercentageCalibImagesCaptured(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        workDone = [0] * len(self.nIntrinsicImages)
        for i in range(0, len(self.nIntrinsicImages)-1):
            workDone[i] = self.nIntrinsicImages[i] * \
                100/self.NImagesRequieredForCamera
        return workDone

    def enoguhImagesToCalibrate(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        enough = False
        perc = self.getPercentageCalibImagesCaptured()
        total = (perc[0]+perc[1]+perc[2])/3
        if(total >= 100):
            enough = True
        return True
        # return enough

    '''
    # -------------------Get/Set parameters -----------------------
    '''

    def setCalibrationFrameNumbers(self, nFrames):
        self.nIntrinsicImages = [nFrames, nFrames, nFrames]

    def setPatternSquareSize(self, height, witdh):
        self.patternHeightSize = height
        self.patternWitdhtSize = witdh

    def getCalibrationFrameNumbers(self):
        # Just because the same number of frames is used for each camera
        return self.nIntrinsicImages[0]

    def getPatternHeightSize(self):
        return self.patternHeightSize

    def getPatternWitdhSize(self):
        return self.patternWitdhtSize
    '''
    # ------------------Write/Read settings ----------------------
    '''

    def readSettingsFile(self):

        #try:
        # TODO: Changed path here too
        fs = cv2.FileStorage("./CalibrationMain_Module/data/calib/cameraCalibrationParams.xml", cv2.FILE_STORAGE_READ)
            #break
        '''except SystemError:
            print("System error: {0}".format(SystemError))
            print(str(SystemError))
            traceback.print_tb(SystemError)
            #logger.exception('Failed: ' + str(SystemError))'''

        self.extrinsic_Translation[0] = np.transpose(fs.getNode("cams_A_B_translation").mat())
        self.extrinsic_Rotation[0] = np.transpose(fs.getNode("cams_A_B_rotation").mat())
        self.extrinsic_Translation[1] = np.transpose(fs.getNode("cams_B_C_translation").mat())
        self.extrinsic_Rotation[1] = np.transpose(fs.getNode("cams_B_C_rotation").mat())
        self.extrinsic_Translation[2] = np.transpose(fs.getNode("cams_A_C_translation").mat())
        self.extrinsic_Rotation[2] = np.transpose(fs.getNode("cams_A_C_rotation").mat())

        fs.release()


        fs = cv2.FileStorage(self.settingsFileName,
                             cv2.FILE_STORAGE_READ)
        if(not fs.isOpened()):
            print("Error opening settings file")
            return 0
        #print("Opened Settings file")
        self.nIntrinsicImages = fs.getNode("nIntrinsicImages").mat()
        self.patternHeightSize = (fs.getNode("patternHeightSize").mat()[0][0])
        self.patternWitdhtSize = (fs.getNode("patternWitdhtSize").mat()[0][0])
        self.cam_vec_type = fs.getNode("cam_vec_type").mat().reshape((1,3))[0]
        fs.release()

        for i in range(0, 3): #TODO: Change folder here too
            fs = cv2.FileStorage("./CalibrationMain_Module/data/calib/camera" +
                                 str(i) + "CalibrationParams.xml",
                                 cv2.FILE_STORAGE_READ)
            if(not fs.isOpened()):
                #print("Error. Loading XML calibParams")
                print("Warning: No calibration XML loaded")
            else:
                #TODO: Fix exception when files are not right - FIXED
                #print("When tries to open cal files")
                #print(i)
                try:
                    self.m[i] = fs.getNode("cam"+str(i)+"_matrix").mat() #isMap exception ocurs here
                    self.d[i] = fs.getNode("cam"+str(i)+"_dist").mat()
                except:
                    pass

            fs.release()

        '''
        self.m[0] = np.matrix([[6.5408667722874100e+02, 0., 3.4981844133366565e+02], 
                     [0., 6.2873735301601585e+02, 1.5144183279590430e+02], 
                     [0., 0., 1.]])
        self.d[0] = np.matrix([3.1875121297986503e-02, 
                    -4.3162039421570408e-01, 
                    -1.8346087686617586e-02, 
                    1.2638712721751431e-02,
                    5.6325132998988325e-01])
        '''

    def writeSettingsFile(self):

        fs = cv2.FileStorage(self.settingsFileName,
                             cv2.FILE_STORAGE_WRITE)
        fs.write("nIntrinsicImages",
            np.array((self.nIntrinsicImages)))
        fs.write("patternHeightSize",
            np.array([self.patternHeightSize, 0]))
        fs.write("patternWitdhtSize",
            np.array([self.patternWitdhtSize, 0]))
        fs.write("cam_vec_type",
            np.array(self.cam_vec_type))
        fs.release()
