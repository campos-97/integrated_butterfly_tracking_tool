# -*- coding: utf-8 -*-
"""
Butterflies application - SIP-Lab.

MainWindow.py
Created: 2018/08/10
Author: Abraham Arias Chinchilla
Modified by Diego Solís Jiménez, IIS-2019
Debbuger, optimizer, and maintainer: Diego Solís Jiménez, 2019.

"""
#TODO: Añadir inclusiones aquí-----------------------------
from PyQt5.QtWidgets import (QMainWindow, QAction, QMenu,
                             QMenuBar, QFileDialog, QVBoxLayout)
from .GUI.SettingsDialog import SettingsDialog
from PyQt5.QtCore import pyqtSlot, QThreadPool
from PyQt5.uic import loadUi
import matplotlib.pyplot as plt
from .thread.Worker import Worker
# New imports to for new GUI and So
from ResourceCapture_Module.flir_cameras_setup.src.syncFLIR import *
from Triagulation_Module.triangulation_module import plot3DPosition
from PyQt5.QtGui import QDesktopServices as qtservices
from PyQt5.QtCore import QUrl




class MainWindow(QMainWindow):
    """Main window to display to application.

    The __init__ method may be documented in either the class level
    docstring, or as a docstring on the __init__ method itself.

    Note:
        A

    Args:
        msg (str): A
        code (int): A.

    Attributes:
        msg (str): A.
        code (int): A.

    """

    def __init__(self, coreLogic):
        """Use the constructor to setup the qt interface.

        Args:
            coreLogic (AppCore): Object to handle the
            algorithms logic.

        Returns:

        """
        super(MainWindow, self).__init__()

        #Si se corre desde pycharm las rutas cambian. Por lo que hay que descomentar esta linea y comentar la siguiente
        loadUi('CalibrationMain_Module/designer/MainWindowUI.ui', self)
        #TODO: Definición de atributos y demás para el objeto de la interfaz principal.
        self.show()
        self.alg = coreLogic
        self.readyToClose = True
        self.selectedCamera = 0
        self.typeOfCalibration = "STANDBY"
        self.pendingImageStorage = False
        self.pendingRunninCalibrationAlgo = False
        self.rotationInstruction = 0
        self.createMenus()
        self.threadpool = QThreadPool()
        self.createSignals()
        # Ring Buffer initialization
        self.ringBuffer = RingBuffer(60)  # can keep up to 2 seconds of recording at 30 fps
        # FLAG for saving or only mem using
        self.save_mem_flag = False
        # Variables for recording control
        self.time_recording = 2 #TODO: Change to continuous recording. Sin usar: El módulo de captura no lo soporta
        self.amount_frames = 90
        #Variable to stop recording
        self.stop_recording = [False]
        self.rootPath = "./exp_files/" #Ruta raíz para medios
        #Change these paths. Python is weird. ->Fixed: Ahora los paths son relativos. No es necesario cambiarlos
        self.reconstructionPath = "./Triagulation_Module/triangulationTest.txt"
        self.calibrationPath = "./Triagulation_Module/CalibrationXML/"
        self.calName = ["1"]
        self.mediaPath = ["1"]


    def closeEvent(self, event):
        """Funtion to close completly the application.

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param event:

        """
        self.tabWidget_Main.setCurrentIndex(1)
        while not self.readyToClose:
            pass
        self.alg.closeCameras()
        QMainWindow.closeEvent(self, event)

    def createMenus(self):
        """Create menu options and assign actions.

        Args:
        Returns:
        """
        # -----Menu-----
        menuMenu = self.menubar.addMenu('Menú')
        settingsButton = QAction('Configuración', self)
        settingsButton.triggered.connect(self.openSettingsWindow)
        settingsButton.setShortcut("Ctrl+E")
        menuMenu.addAction(settingsButton)
        # -----Help-----
        #menuMenu = self.menubar.addMenu('Ayuda')
        helpButton = QAction('Ayuda', self)
        helpButton.triggered.connect(self.openHelpWindow)
        helpButton.setShortcut("Ctrl+H")
        menuMenu.addAction(helpButton)

    def onTabChange(self, tabIndex):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param tabIndex:

        """
        #worker = 0

        if tabIndex !=2 and tabIndex !=3: #Para asegurar que se de este estado
            self.typeOfCalibration = "STANDBY"
        else:
            worker = Worker(self.calibrationLogic)
            self.threadpool.start(worker)
            # worker.signals.error.connect(self.erroOpeningEnoughCameras)
            # worker.signals.result.connect(self.erroOpeningEnoughCameras2)
            # worker.signals.progress.connect(self.erroOpeningEnoughCameras3)


    def updateConfigurationMode(self, tabIndex):
        #Isn't this fixing the prior problem? Why is it freezing? -> FIXED
        #self.progressBar_CalibrationProcess.setVisible(True)
        if tabIndex == 0:

            self.progressBar_CalibrationProcess.setVisible(False)
            self.pushButton_captureCameraPic.setVisible(False)
            self.typeOfCalibration = "STANDBY"

        elif tabIndex == 1:

            self.pushButton_captureCameraPic.setVisible(False)
            self.progressBar_CalibrationProcess.setVisible(False)
            self.typeOfCalibration = "ROTATION"

        elif tabIndex == 2:
            self.typeOfCalibration = "DISTORTION"
            self.progressBar_CalibrationProcess.setVisible(False)
            self.pushButton_captureCameraPic.setVisible(True)

    def calibrationLogic(self, progress_callback):
        print("Starting CalibrationLogic")
        #COMPLETED: Fixed crash problem after calibration. The files are generated correctly and the app doesn't crash
        if self.alg.openCameras() < 1:
            print("Not able to open a single camera")
            self.errorOpeningEnoughCameras()
            return -1

        self.readyToClose = False
        inst = 0
        error = 0
        while self.tabWidget_Main.currentIndex() == 2:
            try:
                originals, imgWithInst, patternsFound, inst, error = self.alg.captureAllImages(
                    self.selectedCamera,
                    self.typeOfCalibration,
                    self.radioButton_PairsCalib.isChecked())
                # OpenGl object to draw
                self.updateCalibrationImagesLabels(imgWithInst, error)  # update cameras
            except TypeError:
                print("Camera not connected")
            if self.typeOfCalibration == "DISTORTION":
                try:
                    if (self.pendingImageStorage is True and
                            sum(patternsFound) >= 1 and
                            self.radioButton_IndividualCalib.isChecked()):
                        # Thread to save images
                        self.threadCallSaveImages(originals)
                    elif (self.pendingImageStorage is True and
                          sum(patternsFound) > 1 and
                          not self.radioButton_IndividualCalib.isChecked()):
                        self.threadCallSaveImages(originals)
                    if (self.pendingRunninCalibrationAlgo is True and
                            self.alg.settings.enoguhImagesToCalibrate() is True):
                        self.threadExecuteCalibrationAlgorithm()
                except:
                    pass
            elif self.typeOfCalibration == "ROTATION":
                try:
                    self.painterDrawRotationInstruction(inst, error)
                except:
                    pass
            # else:
            # print("This is very inconvenient -Hannibal Barca")
        while self.tabWidget_Main.currentIndex() == 3:
            try:
                originals, imgWithInst, patternsFound, inst, error = self.alg.captureAllImages(
                    self.selectedCamera,
                    self.typeOfCalibration,
                    self.radioButton_PairsCalib.isChecked())
                # OpenGl object to draw
                self.updateCalibrationImagesLabels(imgWithInst, error)  # update cameras
            except TypeError:
                print("Camera not connected can't initialize calibration ")
        self.alg.closeCameras()
        self.readyToClose = True
        return 0

    def threadCallSaveImages(self, originalImg): #Llamada de almacenamiento de imagenes

        savingImagesWorker = Worker(
            self.savingCalibrationImages, originalImg, self.selectedCamera)
        savingImagesWorker.signals.finished.connect(
            self.doneSavingCalibrationImages)
        savingImagesWorker.signals.error.connect(
            self.doneSavingCalibrationImagesError)
        self.threadpool.start(savingImagesWorker)
        self.pendingImageStorage = False

    def threadExecuteCalibrationAlgorithm(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        #Here is the calibration initiation

        runningCalibAlgWorker = Worker(self.runningCalibrationAlgorithm)
        runningCalibAlgWorker.signals.progress.connect(
            self.updateIntrinsicCalibrationProgress)
        self.threadpool.start(runningCalibAlgWorker)
        self.pendingRunninCalibrationAlgo = False

    def savingCalibrationImages(
            self,
            imgs,
            cameraIDSelected,
            progress_callback):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param imgs:
            :param cameraIDSelected:
            :param progress_callback:

        """
        typeCalib = 1
        if self.radioButton_IndividualCalib.isChecked():
            typeCalib = 0

        bool = self.alg.SaveCalibrationImages(imgs, cameraIDSelected, typeCalib)

    def runningCalibrationAlgorithm(self, progress_callback):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param progress_callback:

        """
        #e = self.alg.runCalibrationAlg(progress_callback,self.calName) #para pasar parametro de dirección
        e = self.alg.runCalibrationAlg(progress_callback)
        if e == 0:
            self.label_CalibrationInfo.setText(
                "Calibration executed correctly"
            )
        #else:
            #self.label_CalibrationInfo.setText("Calibration Error")

    '''
     --------------------- PYQT Interface Setup ---------------------
    '''
    #TODO: Añadir las señales para los elementos de interfaz aquí.
    def createSignals(self):
        """Use this function to create all the signals.

        Args:

        Returns:

        """
        # Signals and slots
        self.tabWidget_Main.currentChanged.connect(
            self.onTabChange)  # changed!
        self.pushButton_captureCameraPic.setVisible(False)
        self.progressBar_CalibrationProcess.setVisible(False)
        self.pushButton_captureCameraPic.clicked.connect(
            self.slot_pushButton_captureCameraPic_clicked)
        self.pushButton_SelectCam0.clicked.connect(
            self.slot_pushButton_SelectCam0_clicked)
        self.pushButton_SelectCam1.clicked.connect(
            self.slot_pushButton_SelectCam1_clicked)
        self.pushButton_SelectCam2.clicked.connect(
            self.slot_pushButton_SelectCam2_clicked)
        self.pushButton_ExecuteCalibrationAlgo.clicked.connect(
            self.slot_pushButton_ExecuteCalibrationAlgo_clicked)
        self.tabWidget_ModeConfiguration.currentChanged.connect(
            self.updateConfigurationMode)
        self.pushButton_test.clicked.connect(self.movePainter)
        # Self added signals to connect new GUI elements

        # Comenzar a grabar medios
        self.startRecording.clicked.connect(
            self.slot_startRecording_clicked)
        #Detener grabación de medios
        self.stopRecording.clicked.connect(
            self.slot_stopRecording_clicked)
        #Reiniciar la grabación de medios
        self.restartRecording.clicked.connect(
            self.slot_restartRecording_clicked)
        # Comenzar la reconsutrucción de trayectoria
        self.startRecording_2.clicked.connect(
            self.slot_startRecording_2_clicked)

        #Seleccionar archivo a reconstruir
        self.chooseReconstruction_button.clicked.connect(
            self.slot_chooseReconstruction_button_clicked)
        #Seleccionar carpeta de medios
        self.chooseMediaFile_button.clicked.connect(
            self.slot_chooseMediaFile_clicked)

        #Seleccionar calibración
        self.chooseCalibration_button.clicked.connect(
            self.slot_chooseCalibration_button_clicked)
    def updateIntrinsicCalibrationProgress(self, n):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param n:

        """
        self.progressBar_CalibrationProcess.setValue(n)

    def doneSavingCalibrationImages(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        workDone = self.alg.getAmountOfCalibrationWorkDoneForCamera()
        self.progressBar_Cam0.setValue(workDone[0])
        self.progressBar_Cam1.setValue(workDone[1])
        self.progressBar_Cam2.setValue(workDone[2])
        self.label_CalibrationInfo.setText(
            "1 new image capture and saved. Done")

    def updateCalibrationImagesLabels(self, imgs, error):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param imgs:
            :param error:

        """
        if self.tabWidget_Main.currentIndex() == 3:
            self.label_camera0In_2.setPixmap(imgs[0])
            self.label_camera1In_2.setPixmap(imgs[1])
            self.label_camera2In_2.setPixmap(imgs[2])
        else:
            #self.label_CalibrationInfo.setText(str(error))
            self.label_camera0In.setPixmap(imgs[0])
            self.label_camera1In.setPixmap(imgs[1])
            self.label_camera2In.setPixmap(imgs[2])
            self.label_camera_with_inst.setPixmap(imgs[3])

    def updateCalibrationImagesLabels_capture(self, imgs, error):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param imgs:
            :param error:

        """
        # self.label_CalibrationInfo.setText(str(error))
        self.label_camera0In_2.setPixmap(imgs[0])
        self.label_camera1In_2.setPixmap(imgs[1])
        self.label_camera2In_2.setPixmap(imgs[2])
        # self.label_camera_with_inst.setPixmap(imgs[3])

    def doneSavingCalibrationImagesError(self, err):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param err:

        """
        print("-----------------------------------")
        pass

    def openSettingsWindow(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        setsDia = SettingsDialog(self, self.alg.settings)
        return

    def openHelpWindow(self):
        """
        w = PictureFlow()
        d = gi.Repository.QtPoppler.Document.load('file.pdf')
        d.setRenderHint(QtPoppler.Poppler.Document.Antialiasing and QtPoppler.Poppler.Document.TextAntialiasing)

        page = 0
        pages = d.numPages() - 1
        while page < pages:
            page += 1
            print (page)
            w.addSlide(d.page(page).renderToImage())
        w.show()*/
        """
        #qtservices.openUrl(QUrl.fromLocalFile("./Docs/manual.pdf"))
        try:
            qtservices.openUrl(QUrl("./Docs/manual.pdf")) #Para abrir el manual
        except:
            print("No PDF viewer on system")
        return
    def errorOpeningEnoughCameras(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.label_StartInfo.setText("Error: Opening enough cameras")

    #Here is flir_capture method. Apparently working, more deep testing required
    def startFLIRRecording(self):
        # Define controller
        controller = flir_controller(2, 30)

        # Define cameras and list
        cam_master = flir_camera('18285942', True)    # Master
        cam_slave_1 = flir_camera('18286263', False)  # Slave
        cam_slave_2 = flir_camera('18286257', False)  # Slave
        cameras = [cam_master, cam_slave_1, cam_slave_2] #TODO: Change for the next commented line in case of only two cammeras conencted
        #cameras = [cam_master, cam_slave_1]
        controller.get_cameras(cameras)
        controller.init_cameras()
        controller.start_acquisition()
        #workerCapture = Worker(controller.save_images(self.ringBuffer, self.save_mem_flag, self.time_recording, self.amount_frames,self.stop_recording,self.calName))
        self.save_mem_flag = str(self.store.currentText())
        print("self.save_mem_flag")
        print(self.save_mem_flag)
        controller.save_images(self.ringBuffer, self.save_mem_flag, self.time_recording, self.amount_frames,self.stop_recording,self.calName)
        #self.threadpool.start(workerCapture)
        controller.end_acquisition()
        controller.end_system()
        print("Everything done")

    def stopFLIRRecording(self):
        self.stop_recording[0] = True

    def continueFLIRRecording(self):
        self.stop_recording[0] = False
        self.startFLIRRecording()

    def beginReconstruction(self):
        plt_fig = plt.figure()

        # canvas = FigureCanvasTkAgg(fig, master=top)
        # ax = fig.add_subplot(111, projection='3d')
        ax = plt_fig.add_subplot(111, projection='3d')
        ax.set_title(self.reconstructionPath)
        test = open(self.reconstructionPath,'r')
        temp = test.readline()
        while temp != '':
            temp.replace("\n", "")
            temp = temp.split(' ')
            temp = plot3DPosition(float(temp[0]), float(temp[1]), float(temp[2]), float(temp[3]), ax,self.calibrationPath)
            #print("temp")
            #print(temp)
            #print("------")
            temp = test.readline()

        plt.show()

    def chooseReconstructionFile(self):
        dlg = QFileDialog.getOpenFileName()
        #print("dlg")
        #print(dlg)
        if dlg[0] != '':
            self.reconstructionPath = dlg[0]
        #dlg.setFileMode(QFileDialog.AnyFile)
        #dlg.setFilter("Text files (*.txt)")
        #filenames = QStringList()

        #if dlg.exec_():
        #    filenames = dlg.selectedFiles()

    def chooseMediaFile(self):
        dlg = QFileDialog.getExistingDirectory()
        print(dlg)
        #print(str(self.store.currentText()))
        if dlg[0] != '':
            self.mediaPath[0] = dlg[0]

    def chooseCalibration(self):
        dlg = QFileDialog.getExistingDirectory()
        if dlg[0] != '':
            self.calName = dlg[0]
    '''
     ------------------------------ PYQT SLOTS ------------------------------
    '''
    #TODO: Añadir @pyqtSlot() en esta sección
    @pyqtSlot()
    def slot_pushButton_SelectCam0_clicked(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.selectedCamera = 0

    @pyqtSlot()
    def slot_pushButton_SelectCam1_clicked(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.selectedCamera = 1

    @pyqtSlot()
    def slot_pushButton_SelectCam2_clicked(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.selectedCamera = 2

    @pyqtSlot()
    def slot_pushButton_captureCameraPic_clicked(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        self.pendingImageStorage = True

    @pyqtSlot()
    def slot_pushButton_ExecuteCalibrationAlgo_clicked(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        #TODO: here goes calibration
        self.progressBar_CalibrationProcess.setVisible(True)
        self.pendingRunninCalibrationAlgo = True

    #@pyqtSlot()
    def movePainter(self):
        self.paint.moveSomething(self.rotationInstruction)

    #@pyqtSlot()
    def painterDrawRotationInstruction(self, inst, error):
        self.rotationInstruction = inst
        # self.paint.moveSomething(self.rotationInstruction)

    @pyqtSlot()
    def slot_startRecording_clicked(self):
        #print("Started Recording")
        self.startFLIRRecording()  # TODO: Keep it for both options. By setting timer and by continuous recording
    @pyqtSlot()
    def slot_stopRecording_clicked(self):
        self.stopFLIRRecording()
    @pyqtSlot()
    def slot_restartRecording_clicked(self):
        self.continueFLIRRecording()
    @pyqtSlot()
    def slot_chooseMediaFile_clicked(self):
        self.chooseMediaFile()
    @pyqtSlot()
    def slot_startRecording_2_clicked(self):
        #print("Started Reconstruction")
        self.beginReconstruction()
    @pyqtSlot()
    def slot_chooseReconstruction_button_clicked(self):
        #print("Gonna Choose")
        self.chooseReconstructionFile()

    @pyqtSlot()
    def slot_chooseCalibration_button_clicked(self):
        # print("Gonna Choose")
        self.chooseCalibration()
